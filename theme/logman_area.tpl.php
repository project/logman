<?php
/**
 * @file
 * Logman page statistics theme file.
 */
?>
<div id="logman_container" class="<?php print $log_class; ?>">
<div id="logman_icon"><?php print t('Logman'); ?></div>
<div id="logman_icon_close"><?php print t('Close'); ?></div>
<div id="logman_statistics" class="inactive"><?php print $logman_page_statistics; ?></div>
<div class="logman_clear"></div>
</div>
